Morpion
=======

Diagramme de classes
====================

```mermaid
classDiagram
    class Position {
        -x : int
        -y : int
        +x() int
        +y() int
    }

    class Board {
        -width : int
        -height : int
        +place(p : Placed)
        +isOccupied() bool
        +get(p : Position) Tile
        +isBound(p : Position) bool
        +isFull() bool
    }
    class Tile {
    }
    <<interface>> Tile
    Empty <|-- Tile
    Placed <|-- Tile
    class Placed {
        -value : char
        +value() char
    }
    Tile "*" <-- Board
    
    class Game {
        -board : Board
        -players : List~Player~
        -currentPlayer : int
        +board() Board
        +currentPlayer() Player
        +nextPlayer()
        +placeCurrent(p : Position) MoveResult
        +players() List~Player~
    }
    Board <-- Game
    
    class MoveResult {
    }
    <<interface>> MoveResult
    class Result {
        -wins : List~Win~
    }
    class CantPlace {
    }
    Result <|-- MoveResult
    CantPlace <|-- MoveResult
    class Win {
        -positions : List~Position~
        +positions() List~Position~
    }
    
    class WinChecker {   
        +detectFrom(b : Board, p : Position) List~Win~ 
    }
    <<interface>> WinChecker
    Win <.. WinChecker
    Position <.. Win
    Position <.. Board
    MoveResult <.. Game
```