package fr.uca.iut.clfreville2.morpion.cli;

import fr.uca.iut.clfreville2.morpion.game.Game;
import fr.uca.iut.clfreville2.morpion.game.Player;
import fr.uca.iut.clfreville2.morpion.game.Position;
import fr.uca.iut.clfreville2.morpion.win.CantPlace;
import fr.uca.iut.clfreville2.morpion.win.MoveResult;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import java.util.stream.IntStream;

public class CliPlay {

    private Game game;
    private final Scanner scanner = new Scanner(System.in);

    public void play() {
        System.out.print("Specify the number of players: ");
        final int playersCount = readInt();
        final List<Player> players = IntStream.rangeClosed(1, playersCount)
                .mapToObj(i -> new Player("Player" + i))
                .toList();
        game = new Game(players.toArray(Player[]::new));
        game.start();

        final AsciiBoardFormatter formatter = new AsciiBoardFormatter();
        while (!isGameOver() && !game.board().isFull()) {
            formatter.write(game.board(), System.out);
            System.out.println("Player turn: " + game.currentPlayer().name());
            round();
        }
    }

    private boolean isGameOver() {
        return game.players().stream().anyMatch(player -> player.score() > 0);
    }

    private void round() {
        final Position pos = readPos();
        final MoveResult result = game.placeCurrent(pos);
        if (result instanceof CantPlace) {
            System.err.println("Tile already occupied!");
            round();
        }
    }

    private Position readPos() {
        System.out.print("Indicate the coords (x, y) to play, separated by a whitespace: ");
        final int x = readInt();
        final int y = readInt();
        final Position pos = new Position(x, y);
        if (!game.board().isBound(pos)) {
            System.err.println("Out of bounds!");
            return readPos();
        }
        return pos;
    }

    private int readInt() {
        try {
            return scanner.nextInt();
        } catch (InputMismatchException ex) {
            System.err.println("Nah, it's not an int!");
            scanner.next();
            return readInt();
        }
    }
}
