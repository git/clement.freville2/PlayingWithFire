package fr.uca.iut.clfreville2.morpion.game;

/**
 * A union type that is either an {@link Empty empty tyle} or a {@link Placed placed tile}.
 */
public sealed interface Tile permits Empty, Placed {
}
