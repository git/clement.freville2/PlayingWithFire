package fr.uca.iut.clfreville2.morpion.win;

/**
 * A tile that cannot be placed because it is already occupied.
 */
public record CantPlace() implements MoveResult {
}
