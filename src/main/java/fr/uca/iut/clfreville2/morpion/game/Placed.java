package fr.uca.iut.clfreville2.morpion.game;

/**
 * A placed tile.
 */
public record Placed(char x) implements Tile {}
