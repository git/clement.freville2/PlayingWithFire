package fr.uca.iut.clfreville2.morpion.win;

import java.util.Arrays;
import java.util.List;

/**
 * A validated move, that may lead to a win.
 *
 * @param wins The wins from this move.
 */
public record Result(List<Win> wins) implements MoveResult {

    public Result(Win ...wins) {
        this(Arrays.asList(wins));
    }
}
