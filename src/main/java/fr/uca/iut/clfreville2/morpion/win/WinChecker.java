package fr.uca.iut.clfreville2.morpion.win;

import fr.uca.iut.clfreville2.morpion.game.Board;
import fr.uca.iut.clfreville2.morpion.game.Position;

import java.util.List;

/**
 * Something that can check if there is a win including a given position.
 *
 * @see AlignmentWin
 */
@FunctionalInterface
public interface WinChecker {

    /**
     * Detects wins from a given position.
     *
     * @param board The board to use.
     * @param position The position to start from.
     * @return An empty list if there is no win, a list of wins otherwise.
     */
    List<Win> detectFrom(Board board, Position position);
}
