package fr.uca.iut.clfreville2.morpion.win;

import fr.uca.iut.clfreville2.morpion.game.Position;

import java.util.Arrays;
import java.util.List;

/**
 * Something that describe a winning alignment.
 *
 * @param positions The aligned positions.
 */
public record Win(List<Position> positions) {

    public Win(Position ...positions) {
        this(Arrays.asList(positions));
    }
}
