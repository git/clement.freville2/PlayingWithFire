package fr.uca.iut.clfreville2.morpion.win;

/**
 * The result of a placement instruction.
 */
public sealed interface MoveResult permits CantPlace, Result {
}
