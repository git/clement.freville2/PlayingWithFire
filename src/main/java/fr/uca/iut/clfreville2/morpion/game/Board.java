package fr.uca.iut.clfreville2.morpion.game;

import java.util.Arrays;

/**
 * A tic-tac-toe board.
 */
public class Board {

    private final int width, height;
    private final Tile[] tiles;

    /**
     * Construct a new board with the given dimensions.
     *
     * @param width The positive width of the board.
     * @param height The positive height of the board.
     */
    public Board(int width, int height) {
        if (width < 0) {
            throw new IllegalArgumentException("Width must be positive");
        }
        if (height < 0) {
            throw new IllegalArgumentException("Height must be positive");
        }
        this.width = width;
        this.height = height;
        this.tiles = new Tile[width * height];
    }

    public int width() {
        return this.width;
    }

    public int height() {
        return this.height;
    }

    public void place(Position position, Placed placed) {
        this.tiles[position.x() + position.y() * this.width] = placed;
    }

    public boolean isOccupied(Position position) {
        return this.tiles[position.x() + position.y() * this.width] instanceof Placed;
    }

    public Tile get(Position position) {
        if (!isOccupied(position)) {
            return new Empty();
        } else {
            return this.tiles[position.x() + position.y() * this.width];
        }
    }

    public boolean isBound(Position relative) {
        return !isOutOfBound(relative);
    }

    public boolean isOutOfBound(Position relative) {
        return relative.x() < 0 || relative.y() < 0 || relative.x() >= this.width || relative.y() >= this.height;
    }

    public boolean isFull() {
        return Arrays.stream(this.tiles).allMatch(tile -> tile instanceof Placed);
    }
}
