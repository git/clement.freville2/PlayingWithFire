package fr.uca.iut.clfreville2.morpion.game;

/**
 * A tile without any mark.
 */
public record Empty() implements Tile {
}
