package fr.uca.iut.clfreville2.morpion.win;

import fr.uca.iut.clfreville2.morpion.game.Board;
import fr.uca.iut.clfreville2.morpion.game.Empty;
import fr.uca.iut.clfreville2.morpion.game.Position;
import fr.uca.iut.clfreville2.morpion.game.Tile;

import java.util.ArrayList;
import java.util.List;

public record AlignmentWin(int depth) implements WinChecker {

    @Override
    public List<Win> detectFrom(Board board, Position position) {
        List<Win> wins = new ArrayList<>();
        Win vertical = detectFrom(board, position, 0, 1);
        Win horizontal = detectFrom(board, position, 1, 0);
        Win diagonal1 = detectFrom(board, position, 1, 1);
        Win diagonal2 = detectFrom(board, position, -1, 1);
        if (vertical != null) {
            wins.add(vertical);
        }
        if (horizontal != null) {
            wins.add(horizontal);
        }
        if (diagonal1 != null) {
            wins.add(diagonal1);
        }
        if (diagonal2 != null) {
            wins.add(diagonal2);
        }
        return wins;
    }

    private Win detectFrom(Board board, Position start, int ox, int oy) {
        final Tile self = board.get(start);
        if (self.equals(new Empty())) {
            return null;
        }
        while (true) {
            Position rel = new Position(start.x() - ox, start.y() - oy);
            if (board.isOutOfBound(rel) || !board.get(rel).equals(self)) {
                break;
            }
            start = rel;
        }

        final List<Position> positions = new ArrayList<>(this.depth);
        positions.add(start);
        for (int o = 1; o < this.depth; ++o) {
            final Position relative = new Position(start.x() + ox * o, start.y() + oy * o);
            if (board.isOutOfBound(relative) || !board.get(relative).equals(self)) {
                return null;
            }
            positions.add(relative);
        }
        return new Win(positions);
    }
}
