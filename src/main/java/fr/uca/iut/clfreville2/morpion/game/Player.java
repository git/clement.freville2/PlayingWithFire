package fr.uca.iut.clfreville2.morpion.game;

import static java.util.Objects.requireNonNull;

public class Player {

    private final String name;
    private int score;

    public Player(String name) {
        requireNonNull(name);
        if (name.isEmpty()) {
            throw new IllegalArgumentException("Name cannot be empty");
        }
        this.name = name;
    }

    public String name() {
        return this.name;
    }

    public int score() {
        return this.score;
    }

    public void incrementScore() {
        ++this.score;
    }
}
