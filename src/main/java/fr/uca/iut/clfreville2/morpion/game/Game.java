package fr.uca.iut.clfreville2.morpion.game;

import fr.uca.iut.clfreville2.morpion.win.AlignmentWin;
import fr.uca.iut.clfreville2.morpion.win.CantPlace;
import fr.uca.iut.clfreville2.morpion.win.MoveResult;
import fr.uca.iut.clfreville2.morpion.win.Result;
import org.jetbrains.annotations.UnmodifiableView;

import java.util.Arrays;
import java.util.List;

public class Game {

    private boolean started;
    private final Player[] players;
    private int currentPlayer;
    private Board board = new Board(3, 3);

    public Game(Player ...players) {
        if (players.length == 0) {
            throw new IllegalArgumentException();
        }
        this.players = players;
    }

    public void start() {
        if (this.started) {
            throw new IllegalStateException("Game already started");
        }
        this.started = true;
    }

    public Player currentPlayer() {
        return this.players[this.currentPlayer];
    }

    public void nextPlayer() {
        this.currentPlayer = (this.currentPlayer + 1) % this.players.length;
    }

    public Board board() {
        return this.board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public MoveResult placeCurrent(Position position) {
        if (!this.started) {
            throw new IllegalStateException("Game not started");
        }
        if (this.board.isOccupied(position)) {
            return new CantPlace();
        }
        this.board.place(position, new Placed(switch (currentPlayer().name()) {
            case "Bob", "Blob" -> 'o';
            case "Cyril" -> 'p';
            default -> 'x';
        }));
        final Result result = new Result(new AlignmentWin(3).detectFrom(this.board, position));
        if (!result.wins().isEmpty()) {
            currentPlayer().incrementScore();
        }
        nextPlayer();
        return result;
    }

    public @UnmodifiableView List<Player> players() {
        return Arrays.asList(this.players);
    }
}
