package fr.uca.iut.clfreville2.morpion.game;

/**
 * A 2D position with integer coordinates.
 */
public record Position(int x, int y) {
}
