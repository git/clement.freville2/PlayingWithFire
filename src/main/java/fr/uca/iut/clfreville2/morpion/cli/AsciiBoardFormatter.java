package fr.uca.iut.clfreville2.morpion.cli;

import fr.uca.iut.clfreville2.morpion.game.*;

import java.io.PrintStream;

public class AsciiBoardFormatter {

    public void write(Board board, PrintStream stream) {
        for (int y = 0; y < board.height(); ++y) {
            stream.print("|");
            for (int x = 0; x < board.width(); ++x) {
                Tile tile = board.get(new Position(x, y));
                if (tile instanceof Placed placed) {
                    stream.print(placed.x());
                } else {
                    stream.print(' ');
                }
                stream.print("|");
            }
            stream.println();
        }
    }
}
