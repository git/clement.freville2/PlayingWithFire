package fr.uca.iut.clfreville2.morpion.cli;

public final class Main {

    private Main() {}

    public static void main(String[] args) {
        new CliPlay().play();
    }
}
