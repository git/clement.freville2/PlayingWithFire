package fr.uca.iut.clfreville2.morpion.game;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Random;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class BoardTest {

    @ParameterizedTest
    @MethodSource("provideAcceptSize")
    void acceptSize(int width, int height) {
        final Board board = new Board(width, height);
        assertEquals(width, board.width());
        assertEquals(height, board.height());
    }

    @ParameterizedTest
    @MethodSource("provideDenyNegativeSize")
    @SuppressWarnings("SuspiciousNameCombination")
    void denyNegativeSize(int width, int height) {
        assertThrows(IllegalArgumentException.class, () -> new Board(width, height));
        assertThrows(IllegalArgumentException.class, () -> new Board(height, width));
    }

    @ParameterizedTest
    @MethodSource("providePlaceInBounds")
    void placeInBounds(int x, int y) {
        final Board board = new Board(2, 3);
        final Position position = new Position(x, y);
        board.place(position, new Placed('x'));
        assertTrue(board.isOccupied(position));
        assertEquals(new Placed('x'), board.get(position));
    }

    @ParameterizedTest
    @MethodSource("provideEmptyGet")
    void emptyGet(int x, int y) {
        final Board board = new Board(10, 5);
        final Position position = new Position(x, y);
        assertFalse(board.isOccupied(position));
        assertEquals(new Empty(), board.get(position));
    }

    @Test
    void minesweeper() {
        final Board board = new Board(4, 4);
        board.place(new Position(1, 1), new Placed('x'));
        board.place(new Position(1, 2), new Placed('o'));
        board.place(new Position(3, 3), new Placed('x'));
        assertTrue(board.isOccupied(new Position(1, 1)));
        assertEquals(new Placed('x'), board.get(new Position(1, 1)));
        assertFalse(board.isOccupied(new Position(2, 1)));
        assertEquals(new Empty(), board.get(new Position(2, 1)));
        assertFalse(board.isOccupied(new Position(3, 2)));
        assertEquals(new Empty(), board.get(new Position(3, 2)));
        assertTrue(board.isOccupied(new Position(3, 3)));
        assertEquals(new Placed('x'), board.get(new Position(3, 3)));
        assertTrue(board.isOccupied(new Position(1, 2)));
        assertEquals(new Placed('o'), board.get(new Position(1, 2)));
    }

    @Test
    void isBound() {
        final Board board = new Board(4, 6);
        assertTrue(board.isBound(new Position(0, 0)));
        assertTrue(board.isBound(new Position(2, 4)));
        assertTrue(board.isBound(new Position(1, 5)));
        assertTrue(board.isBound(new Position(3, 5)));
        assertFalse(board.isBound(new Position(4, 6)));
        assertFalse(board.isBound(new Position(4, 0)));
        assertFalse(board.isBound(new Position(-9, 3)));
        assertFalse(board.isBound(new Position(0, -1)));
    }

    @Test
    void isNotFull() {
        final Board board = new Board(2, 2);
        assertFalse(board.isFull());
    }

    @Test
    void isNotVeryFull() {
        final Board board = new Board(2, 2);
        board.place(new Position(0, 1), new Placed('x'));
        assertFalse(board.isFull());
    }

    @Test
    void isMaybeVeryFull() {
        final Random random = new Random();
        final Board board = new Board(2, 2);
        board.place(new Position(1, 1), new Placed('x'));
        board.place(new Position(random.nextInt(2), random.nextInt(2)), new Placed('x'));
        assertFalse(board.isFull());
    }

    @Test
    void isFull() {
        final Board board = new Board(2, 2);
        board.place(new Position(0, 0), new Placed('x'));
        board.place(new Position(1, 0), new Placed('p'));
        board.place(new Position(1, 1), new Placed('o'));
        board.place(new Position(0, 1), new Placed('o'));
        assertTrue(board.isFull());
    }

    private static Stream<Arguments> provideAcceptSize() {
        return Stream.of(
                Arguments.of(0, 0),
                Arguments.of(4, 4),
                Arguments.of(3, 3),
                Arguments.of(5, 10),
                Arguments.of(42, 25)
        );
    }

    private static Stream<Arguments> provideDenyNegativeSize() {
        return Stream.of(
                Arguments.of(-1, -1),
                Arguments.of(100, -1),
                Arguments.of(20, -6),
                Arguments.of(-7, 0)
        );
    }

    private static Stream<Arguments> providePlaceInBounds() {
        return Stream.of(
                Arguments.of(0, 0),
                Arguments.of(0, 1),
                Arguments.of(1, 0),
                Arguments.of(1, 1),
                Arguments.of(2, 0),
                Arguments.of(2, 1)
        );
    }

    private static Stream<Arguments> provideEmptyGet() {
        return Stream.of(
                Arguments.of(1, 3),
                Arguments.of(2, 2),
                Arguments.of(3, 0)
        );
    }
}
