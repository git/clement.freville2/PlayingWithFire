package fr.uca.iut.clfreville2.morpion.game;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PositionTest {

    @ParameterizedTest
    @MethodSource("provideHasFixedValue")
    void hasFixedValue(int x, int y) {
        final Position pos = new Position(x, y);
        assertEquals(x, pos.x());
        assertEquals(y, pos.y());
    }

    private static Stream<Arguments> provideHasFixedValue() {
        return Stream.of(
                Arguments.of(0, 0),
                Arguments.of(4, 4),
                Arguments.of(3, -9),
                Arguments.of(10, 8)
        );
    }
}
