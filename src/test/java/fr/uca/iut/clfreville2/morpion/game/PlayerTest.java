package fr.uca.iut.clfreville2.morpion.game;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Random;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PlayerTest {

    @Test
    void denyEmptyName() {
        assertThrows(NullPointerException.class, () -> new Player(null));
        assertThrows(IllegalArgumentException.class, () -> new Player(""));
    }

    @ParameterizedTest
    @MethodSource("provideHasFixedName")
    void hasFixedName(String name) {
        final Player player = new Player(name);
        assertEquals(name, player.name());
    }

    @Test
    void scoreIsInitiallyZero() {
        assertEquals(0, new Player("Martin").score());
    }

    @Test
    void scoreCanBeIncrementedOnce() {
        final Player player = new Player("Pierre");
        player.incrementScore();
        assertEquals(1, player.score());
    }

    @Test
    void scoreCanBeIncrementedTwoTimes() {
        final Player player = new Player("Martin");
        player.incrementScore();
        player.incrementScore();
        assertEquals(2, player.score());
    }

    @Test
    void scoreSeed() {
        final Player player = new Player("Pierre");
        final int n = new Random().nextInt(0, 100);
        for (int i = 0; i < n; ++i) {
            player.incrementScore();
        }
        assertEquals(n, player.score());
    }

    private static Stream<Arguments> provideHasFixedName() {
        return Stream.of(
                Arguments.of("Alice"),
                Arguments.of("Bob")
        );
    }
}
