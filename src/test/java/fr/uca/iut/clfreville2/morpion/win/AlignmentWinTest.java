package fr.uca.iut.clfreville2.morpion.win;

import fr.uca.iut.clfreville2.morpion.game.Board;
import fr.uca.iut.clfreville2.morpion.game.Placed;
import fr.uca.iut.clfreville2.morpion.game.Position;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AlignmentWinTest {

    private Board board;
    private WinChecker checker;

    @BeforeEach
    void setup() {
        checker = new AlignmentWin(3);
        board = new Board(3, 3);
    }

    @Test
    void empty() {
        assertEquals(Collections.emptyList(), checker.detectFrom(board, new Position(0, 0)));
        assertEquals(Collections.emptyList(), checker.detectFrom(board, new Position(1, 2)));
        assertEquals(Collections.emptyList(), checker.detectFrom(board, new Position(2, 0)));
    }

    @Test
    void twoInsteadOfThree() {
        board.place(new Position(0, 1), new Placed('x'));
        board.place(new Position(1, 1), new Placed('x'));
        assertEquals(Collections.emptyList(), checker.detectFrom(board, new Position(0, 1)));
        assertEquals(Collections.emptyList(), checker.detectFrom(board, new Position(1, 1)));
    }

    @ParameterizedTest
    @ValueSource(chars = {'x', 'o', 'p'})
    void verticalAlignement(char c) {
        // _ _ o
        // _ _ o
        // _ _ o
        final List<Position> aligned = IntStream.range(0, 3)
                .mapToObj(y -> new Position(2, y))
                .toList();
        for (Position position : aligned) {
            board.place(position, new Placed(c));
        }
        final Win win = new Win(aligned);
        assertEquals(Collections.emptyList(), checker.detectFrom(board, new Position(0, 0)));
        assertEquals(Collections.emptyList(), checker.detectFrom(board, new Position(1, 0)));
        assertEquals(List.of(win), checker.detectFrom(board, new Position(2, 0)));
    }


    @Test
    void diagonalAlignement() {
        // x _ o
        // x o x
        // o _ _
        final Placed x = new Placed('x');
        final Placed o = new Placed('o');
        board.place(new Position(0, 0), x);
        board.place(new Position(0, 1), x);
        board.place(new Position(0, 2), o);
        board.place(new Position(1, 1), o);
        board.place(new Position(2, 0), o);
        board.place(new Position(2, 1), x);
        final Win win = new Win(new Position(2, 0), new Position(1, 1), new Position(0, 2));
        assertEquals(Collections.emptyList(), checker.detectFrom(board, new Position(0, 0)));
        assertEquals(Collections.emptyList(), checker.detectFrom(board, new Position(0, 1)));
        assertEquals(List.of(win), checker.detectFrom(board, new Position(0, 2)));
    }

    @Test
    void diagonalAlignement2() {
        // x o o
        // x x x
        // o o x
        final Placed x = new Placed('x');
        final Placed o = new Placed('o');
        board.place(new Position(0, 0), x);
        board.place(new Position(0, 1), x);
        board.place(new Position(0, 2), o);
        board.place(new Position(1, 0), o);
        board.place(new Position(1, 1), x);
        board.place(new Position(1, 2), o);
        board.place(new Position(2, 0), o);
        board.place(new Position(2, 1), x);
        board.place(new Position(2, 2), x);
        final Win diagonalWin = new Win(new Position(0, 0), new Position(1, 1), new Position(2, 2));
        final Win horizontalWin = new Win(new Position(0, 1), new Position(1, 1), new Position(2, 1));
        assertEquals(List.of(diagonalWin), checker.detectFrom(board, new Position(0, 0)));
        assertEquals(List.of(horizontalWin), checker.detectFrom(board, new Position(0, 1)));
        assertEquals(List.of(horizontalWin, diagonalWin), checker.detectFrom(board, new Position(1, 1)));
        assertEquals(List.of(horizontalWin), checker.detectFrom(board, new Position(2, 1)));
    }
}
