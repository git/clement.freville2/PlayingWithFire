package fr.uca.iut.clfreville2.morpion.game;

import fr.uca.iut.clfreville2.morpion.win.CantPlace;
import fr.uca.iut.clfreville2.morpion.win.Result;
import fr.uca.iut.clfreville2.morpion.win.Win;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class GameTest {

    @Test
    void denyEmptyGame() {
        assertThrows(IllegalArgumentException.class, Game::new);
    }

    @Test
    void startGameOnlyOneTime() {
        Game game = new Game(new Player("Alice"));
        game.start();
        assertThrows(IllegalStateException.class, () -> game.start());
    }

    @Test
    void cantPlayerIfNotStarted() {
        Game game = new Game(new Player("Alice"));
        assertThrows(IllegalStateException.class, () -> game.placeCurrent(new Position(1, 2)));
    }

    @Test
    void getCurrentPlayer() {
        final Player alice = new Player("Alice");
        final Player bob = new Player("Bob");
        Game game = new Game(alice, bob);
        game.start();
        assertSame(alice, game.currentPlayer());
    }

    @Test
    void nextPlayer() {
        final Player alice = new Player("Alice");
        final Player bob = new Player("Bob");
        final Player camille = new Player("Camille");
        Game game = new Game(alice, bob, camille);
        game.start();
        game.nextPlayer();
        assertSame(bob, game.currentPlayer());
    }

    @Test
    void comeBackToFirstPlayer() {
        final Player alice = new Player("Alice");
        final Player bob = new Player("Bob");
        Game game = new Game(alice, bob);
        game.start();
        game.nextPlayer();
        game.nextPlayer();
        assertSame(alice, game.currentPlayer());
    }

    @Test
    void getBoard() {
        final Game game = new Game(new Player("Alice"));
        final Board board = new Board(10, 10);
        game.setBoard(board);
        assertSame(board, game.board());
    }

    @Test
    void playerPlace() {
        final Game game = new Game(new Player("Alice"));
        game.start();
        game.placeCurrent(new Position(1, 2));
        assertEquals(new Placed('x'), game.board().get(new Position(1, 2)));
    }

    @Test
    void playerPlaceDifferentPlayer() {
        final Game game = new Game(new Player("Alice"), new Player("Bob"));
        game.start();
        game.placeCurrent(new Position(0, 0));
        game.placeCurrent(new Position(2, 2));
        assertEquals(new Placed('o'), game.board().get(new Position(2, 2)));
    }

    @Test
    void workWithThreePlayers() {
        final Game game = new Game(new Player("Albert"), new Player("Blob"), new Player("Cyril"));
        game.start();
        game.placeCurrent(new Position(1, 2));
        game.placeCurrent(new Position(2, 1));
        game.placeCurrent(new Position(2, 2));
        game.placeCurrent(new Position(0, 0));
        assertEquals(new Placed('p'), game.board().get(new Position(2, 2)));
        assertEquals(new Placed('x'), game.board().get(new Position(0, 0)));
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 2})
    void scoreVertically(int x) {
        final Game game = new Game(new Player("Ema"));
        game.start();
        assertEquals(new Result(), game.placeCurrent(new Position(x, 0)));
        assertEquals(new Result(), game.placeCurrent(new Position(x, 1)));
        assertEquals(new Result(new Win(
                new Position(x, 0),
                new Position(x, 1),
                new Position(x, 2)
        )), game.placeCurrent(new Position(x, 2)));
    }

    @Test
    void scoreDiagonally() {
        final Game game = new Game(new Player("Victor"));
        game.start();
        assertEquals(new Result(), game.placeCurrent(new Position(0, 0)));
        assertEquals(new Result(), game.placeCurrent(new Position(2, 2)));
        assertEquals(new Result(new Win(
                new Position(0, 0),
                new Position(1, 1),
                new Position(2, 2)
        )), game.placeCurrent(new Position(1, 1)));
    }

    @ParameterizedTest
    @ValueSource(booleans = {false, true})
    void addScoreToCorrectPlayer(boolean secondPlayerWin) {
        final Player alice = new Player("Alice");
        final Player bob = new Player("Bob");
        final Game game = new Game(alice, bob);
        game.start();
        if (secondPlayerWin) {
            game.placeCurrent(new Position(2, 1));
        }
        game.placeCurrent(new Position(0, 0));
        game.placeCurrent(new Position(1, 1));
        game.placeCurrent(new Position(0, 1));
        game.placeCurrent(new Position(2, 2));
        game.placeCurrent(new Position(0, 2));
        if (secondPlayerWin) {
            assertEquals(0, alice.score());
            assertEquals(1, bob.score());
        } else {
            assertEquals(1, alice.score());
            assertEquals(0, bob.score());
        }
    }

    @Test
    void exposePlayers() {
        final Player rami = new Player("Rami");
        final Player rami2 = new Player("Rami2");
        final Game game = new Game(rami, rami2);
        assertEquals(List.of(rami, rami2), game.players());
    }

    @Test
    void cantPlaceTwoTimes() {
        final Game game = new Game(new Player("Alice"), new Player("Bob"));
        game.start();
        game.placeCurrent(new Position(0, 0));
        assertEquals(new CantPlace(), game.placeCurrent(new Position(0, 0)));
    }
}
